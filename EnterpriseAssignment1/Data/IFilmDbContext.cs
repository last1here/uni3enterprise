﻿using EnterpriseAssignment1.Models;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace EnterpriseAssignment1.Data
{
    public interface IFilmDbContext : IDisposable
    {
        DbSet<Film> Films { get; set; }

        int SaveChanges();

        Task<int> SaveChangesAsync();
        // DbSet<Film> Actors { get; set; }
    }
}
