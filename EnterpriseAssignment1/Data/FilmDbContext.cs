﻿using EnterpriseAssignment1.Models;
using System.Data.Entity;
using System.Threading.Tasks;

namespace EnterpriseAssignment1.Data
{
    public class FilmDbContext : DbContext, IFilmDbContext
    {
        public FilmDbContext() : base("name=FilmDbContext") {}

        public DbSet<Film> Films { get; set; }

        public override Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }
    }
}