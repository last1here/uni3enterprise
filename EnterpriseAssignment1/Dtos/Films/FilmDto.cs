﻿using EnterpriseAssignment1.Models;

namespace EnterpriseAssignment1.Dtos.Films
{
    public class FilmDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Year { get; set; }

        public int Length { get; set; }

        public int Rating { get; set; }

        public string Review { get; set; }
    }
}