﻿using EnterpriseAssignment1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;


namespace EnterpriseAssignment1.Dtos.Films
{
    public static class FilmDtoMaps
    {
        public static IQueryable<FilmDto> ToFilmsDto(this IQueryable<Film> source)
        {
            Expression<Func<Film, FilmDto>> createFilmDtos = film => new FilmDto
            {
                Id = film.Id,
                Name = film.Name,
                Year = film.Year,
                Length = film.Length,
                Rating = film.Rating,
                Review = film.Review
            };

            return source.Select(createFilmDtos);
        }

        public static FilmDto ToFilmsDto(this Film film)
        {
            return new FilmDto
            {
                Id = film.Id,
                Name = film.Name,
                Year = film.Year,
                Length = film.Length,
                Rating = film.Rating,
                Review = film.Review
            };
        }

        public static Film ToFilm(this FilmDto source)
        {
            return new Film
            {
                Id = source.Id,
                Name = source.Name,
                Year = source.Year,
                Length = source.Length,
                Rating = source.Rating,
                Review = source.Review
            };
        }
    }
}