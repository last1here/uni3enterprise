﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriseAssignment1.Models
{
    public class Actor
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Dob { get; set; }

        public string Nationality { get; set; }

        public ICollection<Cast> Roles { get; set; }
    }
}