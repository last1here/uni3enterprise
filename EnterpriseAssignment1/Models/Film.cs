﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EnterpriseAssignment1.Models
{
    public class Film
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Year { get; set; }

        public int Length { get; set; }

        public int Rating { get; set; }

        public string Review { get; set; }

        // public ICollection<Cast> Cast { get; set; }
    }
}