﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EnterpriseAssignment1.Models
{
    public class Cast
    {
        public int Id { get; set; }

        public int FilmId { get; set; }
        
        public int ActorId { get; set; }

        public string Role { get; set; }

        public Film Film { get; set; }

        public Film Actor { get; set; }
    }
}