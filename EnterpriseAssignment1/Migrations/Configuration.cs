namespace EnterpriseAssignment1.Migrations
{
    using EnterpriseAssignment1.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EnterpriseAssignment1.Data.FilmDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EnterpriseAssignment1.Data.FilmDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.Films.AddOrUpdate(new Film
            {
                Id = 1,
                Length = 120,
                Year = 1992,
                Name = "1492 - CONQUEST OF PARADISE",
                Rating = 3,
                Review = "The story of Columbus discovering America, beautifully shot in Costa Rica and excellently played by an top-notch cast including Tcheky Karyo, Michael Wincott and Sigourney Weaver as the Queen of Spain. Interestingly the script shows Columbus mercenary side by detailing the deals he wanted in return for discovering this new world before eventually having to return home in disgrace."
            },
            new Film
            {
                Id = 2,
                Length = 60,
                Year = 1997,
                Name = "BRASSED OFF",
                Rating = 5,
                Review = "Drama with a heart, set at the time of a wave of pit closures. As one colliery town faces the end of its pit, the leader of the brass band (Pete Postlethwaite) is determined to keep it together to keep the towns spirit alive. Also features Stephen Tompkinson from TVs Ballykissangel."
            });
        }
    }
}
