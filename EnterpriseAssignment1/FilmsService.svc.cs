﻿using EnterpriseAssignment1.Data;
using EnterpriseAssignment1.Models;
using EnterpriseAssignment1.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Threading.Tasks;

namespace EnterpriseAssignment1
{
    /// <summary>
    /// Soap service for the films api, matches simple interface for requirements. Is a fairly
    /// primative api that could be expanded upon. This service also generates the Wsdl.
    /// </summary>
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class FilmsService
    {
        /// <summary>
        /// Films repository, this is the DAO for the films. This abstracts away from entity
        /// frameworks context inorder to encapsulate the logic and not have linq queries
        /// scattered across the app.
        /// </summary>
        public IFilmRepository filmRespository { get; set; }

        /// <summary>
        /// Constructor for the service set up the film repository.
        /// </summary>
        public FilmsService()
        {
            filmRespository = new FilmRepository(new FilmDbContext());
        }

        /// <summary>
        /// Service method to return the full listings of films.
        /// </summary>
        /// <returns></returns>
        [WebGet(UriTemplate = "/listFilm")]
        [OperationContract]
        public async Task<List<Film>> listFilmsAsync()
        {
            // Handle formating the response to the client.
            HandleResponseFormatting();

            // Return all the films from the repository.
            return await filmRespository.GetAllFilmsAsync();
        }

        [WebInvoke(Method = "POST", UriTemplate = "/addFilm")]
        public async Task<Film> AddFilmAsync(Film film)
        {
            HandleResponseFormatting();

            Debug.WriteLine(System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest.Accept);
            film = await filmRespository.AddFilmAsync(film);
            return film;
        }

        [WebGet(UriTemplate = "/searchFilm?searchTerm={searchTerm}")]
        [OperationContract]
        public async Task<List<Film>> searchFilmsAsync(string searchTerm)
        {
            HandleResponseFormatting();

            List<Film> films = await filmRespository.SearchFilmsAsync(searchTerm);
            return films;
        }

        /// <summary>
        /// Method to format the response to a request, using both the accept header and the 
        /// format query string.
        /// </summary>
        private void HandleResponseFormatting()
        {
            // Get the format query string and accept header.
            string formatQueryStringValue = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["format"];
            string formatAcceptHeader = WebOperationContext.Current.IncomingRequest.Accept;

            // If a format query string was added then we need to check if its xml and if not
            // default to json response.
            if (!string.IsNullOrEmpty(formatQueryStringValue))
            {
                // 
                if (formatQueryStringValue.Equals("xml", StringComparison.OrdinalIgnoreCase))
                {
                    WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Xml;
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Json;
                }
            }
            // If the accept header is xml then just continue because the service will handle it automatically
            // else reset to json.
            else if (!formatAcceptHeader.Equals("application/xml", StringComparison.OrdinalIgnoreCase))
            {
                WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Json;
            }
        }
    }
}
