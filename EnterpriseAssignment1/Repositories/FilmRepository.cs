﻿using EnterpriseAssignment1.Data;
using EnterpriseAssignment1.Dtos.Films;
using EnterpriseAssignment1.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseAssignment1.Repositories
{
    public class FilmRepository : IFilmRepository
    {
        IFilmDbContext db { get; set; }

        public IDbSet<Film> Films
        {
            get
            {
                return db.Films;
            }
        }
        
        public FilmRepository(IFilmDbContext filmDbContext)
        {
            db = filmDbContext;
        }

        public async Task<Film> AddFilmAsync(Film film)
        {
            Films.Add(film);
            await db.SaveChangesAsync();
            return film;
        }

        public async Task<Film> GetFilmByIdAsync(int id)
        {
            return await Films.SingleOrDefaultAsync(f => f.Id == id);
        }

        public async Task<List<Film>> SearchFilmsAsync(string term)
        {
            return await Films.Where(f => f.Name.ToLower().Contains(term.ToLower())).ToListAsync();
        }

        public async Task<List<Film>> GetAllFilmsAsync()
        {
            return await Films.ToListAsync();
        }
    }
}