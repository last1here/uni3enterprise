﻿using EnterpriseAssignment1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseAssignment1.Repositories
{
    public interface IFilmRepository
    {
        Task<Film> AddFilmAsync(Film film);

        Task<Film> GetFilmByIdAsync(int id);

        Task<List<Film>> SearchFilmsAsync(string term);

        Task<List<Film>> GetAllFilmsAsync();

    }
}
