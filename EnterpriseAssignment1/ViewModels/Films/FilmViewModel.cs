﻿using EnterpriseAssignment1.Dtos.Films;

namespace EnterpriseAssignment1.ViewModels.Films
{
    public class FilmViewModel
    {
        public FilmDto Film { get;  set; }
    }
}