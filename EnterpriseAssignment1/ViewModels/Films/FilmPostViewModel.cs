﻿using EnterpriseAssignment1.Dtos.Films;
using System.ComponentModel.DataAnnotations;

namespace EnterpriseAssignment1.ViewModels.Films
{
    public class FilmPostViewModel
    {
        [Required]
        public FilmDto Film { get; set; }
    }
}