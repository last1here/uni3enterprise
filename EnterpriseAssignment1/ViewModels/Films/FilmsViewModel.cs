﻿using EnterpriseAssignment1.Dtos.Films;
using System.Collections.Generic;

namespace EnterpriseAssignment1.ViewModels.Films
{
    public class FilmsViewModel
    {
        public List<FilmDto> Films { get; set; }
    }
}