﻿using EnterpriseAssignment1.Data;
using EnterpriseAssignment1.Dtos.Films;
using EnterpriseAssignment1.ViewModels.Films;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EnterpriseAssignment1.Extensions;
using EnterpriseAssignment1.Models;

namespace EnterpriseAssignment1.Controllers
{
    /// <summary>
    /// Controller to handle interactions with the film entity. All data that is being returned
    /// will be wrapped in a view model and returned as a DTO.
    /// </summary>
    public class FilmsController : ApiController
    {
        /// <summary>
        /// Database context accessor.
        /// </summary>
        private IFilmDbContext db { get; set; }

        /// <summary>
        /// By default the controller will create a new instance of FilmContext.
        /// </summary>
        public FilmsController()
        {
            db = new FilmDbContext();
        }

        /// <summary>
        /// Controller can be injected with its own db context to allow for testing.
        /// </summary>
        /// <param name="filmDbContext"></param>
        public FilmsController(IFilmDbContext filmDbContext)
        {
            db = filmDbContext;
        }

        /// <summary>
        /// Endpoint to return a collection of films in FilmDto form.
        /// </summary>
        /// <param name="search">If term provided films will be filtered to match the
        /// term based on title.</param>
        /// <returns></returns>
        public async Task<FilmsViewModel> GetFilmsAsync([FromUri]string search = "", [FromUri]string order = "")
        {
            IQueryable<FilmDto> query = db.Films.ToFilmsDto();

            // If a search term was entered then adjust the query to search the title to contain the query.
            // This could possibly expanded to search cast name etc.
            if (!string.IsNullOrEmpty(search))
            {
                string lowerSearch = search.ToLower();
                query = query.Where(fDto => fDto.Name.ToLower().Contains(lowerSearch));
            }

            // Sort the query based on the users input.
            query = SortFilmDtos(query, order);

            return new FilmsViewModel
            {
                Films = await query.ToListAsync()
            };
        }

        /// <summary>
        /// Get a film by its id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(FilmViewModel))]
        public async Task<IHttpActionResult> GetFilmAsync(int id)
        {
            FilmDto filmDetailsDto = await db.Films.ToFilmsDto().SingleOrDefaultAsync(fDto => fDto.Id == id);

            // If no film is found return a notFound response.
            if (filmDetailsDto == null)
            {
                return NotFound();
            }

            // If a film was found return it wrapped in the film view model.
            return Ok(new FilmViewModel
            {
                Film = filmDetailsDto
            });
        }

        /// <summary>
        /// Api endpoint to post a new film to be added to the database.
        /// </summary>
        /// <param name="filmData">The information that will be used to create the new film.</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> PostFilmAsync(FilmPostViewModel filmData)
        {
            // Validate that the model has been passed correctly. Will catch if no film has been
            // passed.
            if (!ModelState.IsValid)
            {
                return StatusCode(System.Net.HttpStatusCode.BadRequest);
            }

            // Cast the film dto to a film entity.
            Film newFilm = filmData.Film.ToFilm();

            // Add the new film to the data context. Save the changes afterwards.
            db.Films.Add(newFilm);
            await db.SaveChangesAsync();

            // Return the newly created film with the created response.
            return CreatedAtRoute("DefaultApi", new { id = newFilm.Id }, new FilmViewModel
            {
                Film = newFilm.ToFilmsDto()
            });
        }

        /// <summary>
        /// Get the cast for a given film by its id.
        /// </summary>
        /// <param name="id">Id of the films whos casts to return.</param>
        /// <returns></returns>
        [Route("Api/Films/{id:int}/Cast")] //Map Action and you can name your method with any text
        public string GetFilmCast(int id)
        {
            return "object of id id";
        }

        /// <summary>
        /// Simple helper method to take a string such as "-name,year" and sort the films based
        /// on that string. This will be seperated into a generic method should we require
        /// further entities to be sorted.
        /// </summary>
        /// <param name="filmsQuery">The iqueryable for the films dto.</param>
        /// <param name="order">The string to order the films by.</param>
        /// <returns></returns>
        private IQueryable<FilmDto> SortFilmDtos(IQueryable<FilmDto> filmsQuery, string order)
        {
            // Split the orders so they can be handled one at a time.
            string[] orders = order.Split(',');

            // Store the sorted query as we loop the orders so that we can stack order using the
            // .ThenBy methods of IOrderedQueryable;
            IOrderedQueryable<FilmDto> orderedQuery = null;

            // Loop the ordered string.
            foreach (string orderProperty in orders)
            {
                Expression<Func<FilmDto, object>> keySelector;
                bool descending = false;

                // Simple match the order property using string comparisons, this could be expanded
                // to use expression trees on the entity to remove the need to provice sort cases.
                // For our api this will sufice.
                switch (orderProperty.ToLower())
                {
                    case "name":
                        keySelector = fDto => fDto.Name;
                        break;
                    case "-name":
                        keySelector = fDto => fDto.Name;
                        descending = true;
                        break;
                    case "year":
                        keySelector = fDto => fDto.Year;
                        break;
                    case "-year":
                    default:
                        keySelector = fDto => fDto.Year;
                        descending = true;
                        break;
                }

                // If the query doesnt exist create it adding the new sort, else append the next sort
                // to the existing query.
                if (orderedQuery == null)
                {
                    orderedQuery = descending ? filmsQuery.OrderByDescending(keySelector) : filmsQuery.OrderBy(keySelector);
                }
                else
                {
                    orderedQuery = descending ? orderedQuery.ThenByDescending(keySelector) : orderedQuery.ThenBy(keySelector);
                }
            }

            // If no ordered query was returned just return the original query.
            return orderedQuery == null ? filmsQuery : orderedQuery;
        }

        /// <summary>
        /// Dispose the db context when the api controller is disposed.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            base.Dispose(disposing);
        }
    }
}
